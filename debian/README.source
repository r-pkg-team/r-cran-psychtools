Explanation for binary files inside source package according to
  https://lists.debian.org/debian-devel/2013/09/msg00332.html

Files: data/ability.rda
Documented: man/ability.Rd
  16 ability items scored as correct or incorrect.

Files: data/affect.rda
Documented: man/affect.Rd
  Two data sets of affect and arousal scores as a function of personality and movie conditions

Files: data/bfi.dictionary.rda
Documented: man/bfi.Rd
  25 Personality items representing 5 factors

Files: data/bfi.rda
Documented: man/bfi.Rd
  25 Personality items representing 5 factors

Files: data/blant.rda
Documented: man/blant.Rd
  A 29 x 29 matrix that produces weird factor analytic results

Files: data/blot.rda
Documented: man/blot.Rd
  Bond's Logical Operations Test -- BLOT

Files: data/burt.rda
Documented: man/burt.Rd
  11 emotional variables from Burt (1915)

Files: data/cities.rda
Documented: man/cities.Rd
  Distances between 11 US cities

Files: data/cubits.rda
Documented: man/cubits.Rd
  Galton's example of the relationship between height and 'cubit' or forearm length

Files: data/cushny.rda
Documented: man/cushny.Rd
  The classic data set used by Gossett (publishing as Student) for the introduction of the t-test. The design was a within subjects study with hours of sleep in a control condition compared to those in 3 drug conditions. Drug1 was 06mg of L Hscyamine, Drug 2L and Drug2R were said to be .6 mg of Left and Right isomers of Hyoscine. As discussed by Zabell (2008) these were not optical isomers. The detal1, delta2L and delta2R are changes from the baseline control. \usage{data(cushny) ormat{

Files: data/Damian.rda
Documented: man/psychTools.Rd
  psychTools: datasets and utility functions to accompany the psych package

Files: data/epi.bfi.rda
Documented: man/epi.bfi.Rd
  13 personality scales from the Eysenck Personality Inventory and Big 5 inventory

Files: data/epi.dictionary.rda
Documented: man/epi.Rd
  Eysenck Personality Inventory (EPI) data for 3570 participants

Files: data/epi.rda
Documented: man/epi.Rd
  Eysenck Personality Inventory (EPI) data for 3570 participants

Files: data/epiR.rda
Documented: man/epi.Rd
  Eysenck Personality Inventory (EPI) data for 3570 participants

Files: data/galton.rda
Documented: man/galton.Rd
  Galton's Mid parent child height data

Files: data/heights.rda
Documented: man/heights.Rd
  A data.frame of the Galton (1888) height and cubit data set.

Files: data/holzinger.dictionary.rda
Documented: man/holzinger.swineford.Rd
  A classic data set in psychometrics is that from Holzinger and Swineford (1939). A 4 and 5 factor solution to 24 of these variables problem is presented by Harman (1976), and 9 of these are used by the lavaan package. The two data sets were supplied by Keith Widaman. \usage{data(holzinger.swineford)

Files: data/holzinger.raw.rda
Documented: man/holzinger.swineford.Rd
  A classic data set in psychometrics is that from Holzinger and Swineford (1939). A 4 and 5 factor solution to 24 of these variables problem is presented by Harman (1976), and 9 of these are used by the lavaan package. The two data sets were supplied by Keith Widaman. \usage{data(holzinger.swineford)

Files: data/holzinger.swineford.rda
Documented: man/holzinger.swineford.Rd
  A classic data set in psychometrics is that from Holzinger and Swineford (1939). A 4 and 5 factor solution to 24 of these variables problem is presented by Harman (1976), and 9 of these are used by the lavaan package. The two data sets were supplied by Keith Widaman. \usage{data(holzinger.swineford)

Files: data/income.rda
Documented: man/income.Rd
  US family income from US census 2008

Files: data/iqitems.rda
Documented: man/iqitems.Rd
  16 multiple choice IQ items

Files: data/msq.rda
Documented: man/msq.Rd
  75 mood items from the Motivational State Questionnaire for 3896 participants

Files: data/msqR.rda
Documented: man/msqR.rd
  75 mood items from the Motivational State Questionnaire for 3032 unique participants

Files: data/neo.rda
Documented: man/neo.Rd
  NEO correlation matrix from the NEO_PI_R manual

Files: data/peas.rda
Documented: man/peas.Rd
  Galton`s Peas

Files: data/sai.dictionary.rda
Documented: man/psychTools.Rd
  psychTools: datasets and utility functions to accompany the psych package

Files: data/sai.rda
Documented: man/sai.Rd
  State Anxiety data from the PMC lab over multiple occasions.

Files: data/Schutz.rda
Documented: man/Schutz.Rd
  Shapiro and ten Berge use the Schutz correlation matrix as an example for Minimum Rank Factor Analysis. The Schutz data set is also a nice example of how normal minres or maximum likelihood will lead to a Heywood case, but minrank factoring will not. \usage{data("Schutz")

Files: data/spi.dictionary.rda
Documented: man/psychTools.Rd
  psychTools: datasets and utility functions to accompany the psych package

Files: data/spi.keys.rda
Documented: man/psychTools.Rd
  psychTools: datasets and utility functions to accompany the psych package

Files: data/spi.rda
Documented: man/spi.Rd
  A sample from the SAPA Personality Inventory including an item dictionary and scoring keys.

Files: data/tai.rda
Documented: man/msqR.rd
  75 mood items from the Motivational State Questionnaire for 3032 unique participants

Files: data/USAF.rda
Documented: man/usaf.Rd
  17 anthropometric measures from the USAF showing a general factor

Files: data/vegetables.rda
Documented: man/vegetables.Rd
  Paired comparison of preferences for 9 vegetables

 -- Andreas Tille <tille@debian.org>  Mon, 13 Jan 2020 20:54:48 +0100
